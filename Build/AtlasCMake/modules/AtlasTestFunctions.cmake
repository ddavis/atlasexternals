# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# File collecting CMake code for setting up (CTest) tests.
#

# This function takes care of internally of building the executable that would
# be run as a test. It should only be used by atlas_add_test(...) internally.
#
function( _atlas_add_compiled_test testName )

   # Parse the options given to the function:
   cmake_parse_arguments( ARG "" "" "SOURCES;INCLUDE_DIRS;LINK_LIBRARIES"
      ${ARGN} )

   # Set common compiler options:
   atlas_set_compiler_flags()

   # Get the package/subdirectory name:
   atlas_get_package_name( pkgName )

   # Allow wildcards in the source names:
   file( GLOB _sources RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
      ${ARG_SOURCES} )
   if( NOT _sources )
      message( WARNING "No sources available for test ${exeName}" )
      return()
   endif()

   # Put the files into source groups. So they would show up in a ~reasonable
   # way in an IDE like Xcode:
   atlas_group_source_files( ${_sources} )

   # The name of the executable target (remember that packages often use
   # very generic test names):
   set( exeName "${pkgName}_${testName}" )

   # Declare the executable:
   add_executable( ${exeName} EXCLUDE_FROM_ALL ${_sources} )

   # Set it's properties:
   add_dependencies( ${exeName} ${pkgName}Pkg )
   add_dependencies( Package_${pkgName}_tests ${exeName} )
   set_property( TARGET ${exeName} PROPERTY LABELS ${pkgName} )
   set_property( TARGET ${exeName} PROPERTY FOLDER ${pkgDir}/Tests )
   set_property( TARGET ${exeName} PROPERTY OUTPUT_NAME "${testName}.exe" )
   set_property( TARGET ${exeName} PROPERTY
      RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/test-bin" )
   foreach( _config DEBUG RELEASE MINSIZEREL RELWITHDEBINFO )
      set_property( TARGET ${exeName} PROPERTY
         RUNTIME_OUTPUT_DIRECTORY_${_config}
         "${CMAKE_CURRENT_BINARY_DIR}/test-bin" )
   endforeach()
   target_include_directories( ${exeName} PRIVATE
      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
      $<TARGET_PROPERTY:${pkgName}Pkg,INTERFACE_INCLUDE_DIRECTORIES>
      $<TARGET_PROPERTY:${pkgName}PkgPrivate,INTERFACE_INCLUDE_DIRECTORIES>
      ${ARG_INCLUDE_DIRS} )
   target_link_libraries( ${exeName}
      ${ARG_LINK_LIBRARIES} )

endfunction( _atlas_add_compiled_test )

# This function takes care of internally setting up a simple script that calls
# the script specified by the user, with the arguments given in the
# configuration.
#
function( _atlas_add_script_test testName )

   # Parse the options given to the function:
   cmake_parse_arguments( ARG "" "" "SCRIPT" ${ARGN} )

   # Get the package/subdirectory name:
   atlas_get_package_name( pkgName )

   # Create the test script installation target of the package if it
   # doesn't exist yet:
   if( NOT TARGET ${pkgName}TestScriptInstall )
      add_custom_target( ${pkgName}TestScriptInstall SOURCES
         $<TARGET_PROPERTY:${pkgName}TestScriptInstall,TEST_SCRIPTS> )
      add_dependencies( Package_${pkgName}_tests
         ${pkgName}TestScriptInstall )
      set_property( TARGET ${pkgName}TestScriptInstall
         PROPERTY LABELS ${pkgName} )
      set_property( TARGET ${pkgName}TestScriptInstall
         PROPERTY FOLDER ${pkgDir}/Internals )
   endif()

   # Set up a script ourselves, which executes the specified script, with all
   # of its possible arguments.

   # Get the first element of the list, which should itself always be a script
   # file name. All other possible elements would be the arguments for this
   # script.
   list( GET ARG_SCRIPT 0 _script )
   set( _args ${ARG_SCRIPT} )
   list( REMOVE_AT _args 0 )

   # Construct the command to put into our own script:
   if( IS_ABSOLUTE ${_script} OR
         ( NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${_script} ) OR
         ( IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${_script} ) )
      # The IS_DIRECTORY check allows one to use "python" as the "script" name
      # in a package that has a python/ directory.
      set( _command "${_script}" )
   else()
      set( _command "${CMAKE_CURRENT_SOURCE_DIR}/${_script}" )
   endif()
   while( _args )
      list( GET _args 0 _arg )
      list( REMOVE_AT _args 0 )
      set( _command "${_command} ${_arg}" )
   endwhile()

   # Set up the creation of the script:
   add_custom_command( OUTPUT
      ${CMAKE_CURRENT_BINARY_DIR}/test-bin/${testName}.exe
      COMMAND ${CMAKE_COMMAND} -E make_directory
      ${CMAKE_CURRENT_BINARY_DIR}/test-bin
      COMMAND ${CMAKE_COMMAND} -E echo "${_command}" >
      ${CMAKE_CURRENT_BINARY_DIR}/test-bin/${testName}.exe
      COMMAND chmod 755 ${CMAKE_CURRENT_BINARY_DIR}/test-bin/${testName}.exe
      VERBATIM )
   set_property( TARGET ${pkgName}TestScriptInstall APPEND PROPERTY
      TEST_SCRIPTS ${CMAKE_CURRENT_BINARY_DIR}/test-bin/${testName}.exe )

endfunction( _atlas_add_script_test )

# This function can be used to declare unit tests in a package. It can be
# called in two ways. Either:
#
#   atlas_add_test( TestName SOURCES test/source1.cxx...
#                   [INCLUDE_DIRS Dir1...]
#                   [LINK_LIBRARIES Library1...]
#                   [EXTRA_PATTERNS patterns]
#                   [PRE_EXEC_SCRIPT script]
#                   [POST_EXEC_SCRIPT script]
#                   [ENVIRONMENT env]
#                   [PROPERTIES <name> <value>...]
#                   [NOEXEC] )
#
# Or like:
#
#   atlas_add_test( TestName SCRIPT test/script.sh [arg1...]
#                   [EXTRA_PATTERNS patterns]
#                   [PRE_EXEC_SCRIPT script]
#                   [POST_EXEC_SCRIPT script]
#                   [ENVIRONMENT env]
#                   [PROPERTIES <name> <value>...] )
#
function( atlas_add_test testName )

   # In release recompilation dryrun mode exit now:
   if( ATLAS_RELEASE_RECOMPILE_DRYRUN )
      return()
   endif()

   # Skip the binary building if we are in release mode, but the package is not
   # getting recompiled:
   if( ATLAS_RELEASE_MODE AND NOT ATLAS_PACKAGE_RECOMPILE )
      return()
   endif()

   # Parse the options given to the function:
   set( _booleanArgs NOEXEC )
   set( _singleParamArgs SCRIPT EXTRA_PATTERNS ENVIRONMENT
      PRE_EXEC_SCRIPT POST_EXEC_SCRIPT )
   set( _multiParamArgs SOURCES PROPERTIES )
   cmake_parse_arguments( ARG "${_booleanArgs}" "${_singleParamArgs}"
      "${_multiParamArgs}" ${ARGN} )

   # A sanity check:
   if( ( ARG_SOURCES AND ARG_SCRIPT ) OR
         ( NOT ARG_SOURCES AND NOT ARG_SCRIPT ) )
      message( SEND_ERROR "Script must be called with either SOURCES or "
         "SCRIPT in the arguments" )
      return()
   endif()

   # Set common compiler options:
   atlas_set_compiler_flags()

   # Get the package/subdirectory name:
   atlas_get_package_name( pkgName )

   # Get the package directory:
   atlas_get_package_dir( pkgDir )

   # If the user gave source files, let's build a test executable:
   if( ARG_SOURCES )

      _atlas_add_compiled_test( ${testName}
         SOURCES ${ARG_SOURCES}
         ${ARG_UNPARSED_ARGUMENTS} )

   elseif( ARG_SCRIPT )

      _atlas_add_script_test( ${testName}
         SCRIPT ${ARG_SCRIPT}
         ${ARG_UNPARSED_ARGUMENTS} )

   else()
      message( FATAL_ERROR "Internal logic error detected!" )
   endif()

   # If the test is only to be set up, but not to be executed, stop here:
   if( ARG_NOEXEC )
      return()
   endif()

   # Create the directory in which the unit tests will be executed from:
   file( MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/unitTestRun )

   # Set up the variables for the unit test executor script:
   if( ARG_PRE_EXEC_SCRIPT )
      set( PRE_EXEC_SCRIPT ${ARG_PRE_EXEC_SCRIPT} )
   else()
      set( PRE_EXEC_SCRIPT "# No pre-exec necessary" )
   endif()
   if( ARG_POST_EXEC_SCRIPT )
      set( POST_EXEC_SCRIPT ${ARG_POST_EXEC_SCRIPT} )
   else()
      set( POST_EXEC_SCRIPT "if type post.sh >/dev/null 2>&1; then
    post.sh ${testName} \"${ARG_EXTRA_PATTERNS}\"
fi" )
   endif()

   # Decide where to take bash from:
   if( APPLE )
      # atlas_project(...) should take care of putting it here:
      set( BASH_EXECUTABLE "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/bash" )
   else()
      # Just take it from its default location:
      find_program( BASH_EXECUTABLE bash )
   endif()

   # Generate a test script that will run this unit test in the
   # correct environment:
   find_file( _utSkeleton unit_test_executor.sh.in
      PATH_SUFFIXES scripts PATHS ${CMAKE_MODULE_PATH} )
   configure_file( ${_utSkeleton}
      ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${testName}.sh @ONLY )
   mark_as_advanced( _utSkeleton )
   unset( _utSkeleton )

   # And run this script as the unit test:
   add_test( NAME ${pkgName}_${testName}_ctest
      COMMAND ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${testName}.sh
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/unitTestRun )

   # Set its properties:
   set_tests_properties( ${pkgName}_${testName}_ctest PROPERTIES
      TIMEOUT 120
      LABELS ${pkgName} )
   if( ARG_ENVIRONMENT )
      set_property( TEST ${pkgName}_${testName}_ctest PROPERTY
         ENVIRONMENT "${ARG_ENVIRONMENT}" )
   endif()
   if( ARG_PROPERTIES )
      set_tests_properties( ${pkgName}_${testName}_ctest PROPERTIES
         ${ARG_PROPERTIES} )
   endif()

endfunction( atlas_add_test )
