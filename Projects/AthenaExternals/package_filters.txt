# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthenaExternals.
#
+ External/Blas
+ External/CheckerGccPlugins
+ External/CLHEP
+ External/Coin3D
+ External/FFTW
+ External/FastJet
+ External/FastJetContrib
+ External/GPerfTools
+ External/Gdb
+ External/Geant4
+ External/GoogleTest
+ External/HepMCAnalysis
+ External/Lapack
+ External/lwtnn
+ External/MKL
+ External/Acts
+ External/Simage
+ External/SoQt
+ External/dSFMT
+ External/yampl
- .*
