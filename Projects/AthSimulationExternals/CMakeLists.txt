# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Project file for the AthSimulationExternals project.
#

# The minimum required CMake version:
cmake_minimum_required( VERSION 3.2 FATAL_ERROR )

# If the user didn't specify AtlasCMake_DIR explicitly, pick up that code
# from this repository:
if( "${AtlasCMake_DIR}" STREQUAL "" AND "$ENV{AtlasCMake_DIR}" STREQUAL "" )
   set( AtlasCMake_DIR ${CMAKE_SOURCE_DIR}/../../Build/AtlasCMake )
endif()

# If the user didn't specify LCG_DIR explicitly, pick up the code from this
# repository:
if( "${LCG_DIR}" STREQUAL "" AND "$ENV{LCG_DIR}" STREQUAL "" )
   set( LCG_DIR ${CMAKE_SOURCE_DIR}/../../Build/AtlasLCG )
endif()

# Read in the project's version from a file called version.txt. But let it be
# overridden from the command line if necessary.
file( READ ${CMAKE_SOURCE_DIR}/version.txt _version )
string( STRIP ${_version} _version )
set( ATHSIMULATIONEXTERNALS_PROJECT_VERSION ${_version}
   CACHE STRING "Version of the AthSimulationExternals project to build" )
unset( _version )

# Find the ATLAS CMake code:
find_package( AtlasCMake REQUIRED )

# Set up which LCG version to use:
set( LCG_VERSION_POSTFIX "a"
   CACHE STRING "Post-fix for the LCG version number" )
set( LCG_VERSION_NUMBER 93
   CACHE STRING "LCG version number to use for the project" )
find_package( LCG ${LCG_VERSION_NUMBER} REQUIRED EXACT )

# Set up CTest:
atlas_ctest_setup()

# Declare project name and version
atlas_project( AthSimulationExternals ${ATHSIMULATIONEXTERNALS_PROJECT_VERSION}
   PROJECT_ROOT ${CMAKE_SOURCE_DIR}/../../
   FORTRAN )

# Configure and install the post-configuration file:
configure_file( ${CMAKE_SOURCE_DIR}/PostConfig.cmake.in
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PostConfig.cmake @ONLY )
install( FILES ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PostConfig.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR} )

# Install the export sanitizer script:
install( FILES ${CMAKE_SOURCE_DIR}/atlas_export_sanitizer.cmake.in
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules/skeletons )

# Install the Gaudi CPack configuration module:
install( FILES ${CMAKE_SOURCE_DIR}/GaudiCPackSettings.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )

# Generate the environment setup for the externals, to be used during the build:
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )

# Generate replacement rules for the installed paths:
set( _replacements )
if( NOT "$ENV{NICOS_PROJECT_HOME}" STREQUAL "" )
   get_filename_component( _buildDir $ENV{NICOS_PROJECT_HOME} PATH )
   list( APPEND _replacements ${_buildDir}
      "\${AthSimulationExternals_DIR}/../../../.." )
endif()
if( NOT "$ENV{NICOS_PROJECT_RELNAME}" STREQUAL "" )
   list( APPEND _replacements $ENV{NICOS_PROJECT_RELNAME}
      "\${AthSimulationExternals_VERSION}" )
endif()

# Now generate and install the installed setup files:
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/env_setup_install.sh
   REPLACE ${_replacements} )
install( FILES ${CMAKE_BINARY_DIR}/env_setup_install.sh
   DESTINATION . RENAME env_setup.sh )

# Package up the release using CPack:
atlas_cpack_setup()
