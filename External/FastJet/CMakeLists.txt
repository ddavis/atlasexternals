# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Package building FastJet as part of the offline software build.
#

# Set the package name:
atlas_subdir( FastJet )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Decide whether to request debug symbols from the build:
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "MinSizeRel" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   set( _fastJetExtraConfig "--disable-debug" )
else()
   set( _fastJetExtraConfig "--enable-debug" )
endif()

# Temporary directory for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/FastJetBuild )

# Create the sanitization script:
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeFastJet.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeFastJet.sh
   @ONLY )

# Set up the build of FastJet for the build area:
ExternalProject_Add( FastJet
   PREFIX ${CMAKE_BINARY_DIR}
   URL http://cern.ch/atlas-software-dist-eos/externals/FastJet/fastjet-3.2.2.tar.gz
   URL_MD5 058367d96052f99d6347027a2c39ab4a
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   PATCH_COMMAND patch -p1 < ${CMAKE_CURRENT_SOURCE_DIR}/patches/clang.patch
   CONFIGURE_COMMAND <SOURCE_DIR>/configure
   --prefix=${_buildDir} --enable-shared --disable-static --enable-allcxxplugins
   ${_fastJetExtraConfig}
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeFastJet.sh
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR> )
ExternalProject_Add_Step( FastJet forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of FastJet"
   DEPENDERS download )
ExternalProject_Add_Step( FastJet purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for FastJet"
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_FastJet FastJet )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
