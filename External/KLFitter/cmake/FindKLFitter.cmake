# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Locate the KLFitter external package.
#
# Defines:
#
# The user can set KLFITTER_ROOT to guide the script.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME KLFitter
   INCLUDE_SUFFIXES include INCLUDE_NAMES KLFitter/Fitter.h
   LIBRARY_SUFFIXES lib
   COMPULSORY_COMPONENTS KLFitter )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( KLFitter DEFAULT_MSG KLFITTER_INCLUDE_DIRS
   KLFITTER_LIBRARIES )
mark_as_advanced( KLFITTER_FOUND KLFITTER_INCLUDE_DIR KLFITTER_INCLUDE_DIRS
   KLFITTER_LIBRARIES KLFITTER_LIBRARY_DIRS )
